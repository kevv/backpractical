package controllers

import (
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
)

// Subject all actions to hangle authentication
type Subject struct {
	Db *pg.DB
}

// AddSubject saves a new subject in DB
func (s Subject) AddSubject(subject *models.Subject, userID int) ([]models.Subject, error) {
	subject.AssistantID = userID
	subject.CreatedAt = time.Now()
	subject.UpdatedAt = time.Now()
	if _, err := s.Db.Model(subject).Insert(); err != nil {
		return []models.Subject{}, errors.Wrap("subject.AddSubject", "Couldn't save subject", err)
	}
	var subjects []models.Subject
	if err := s.Db.Model(&subjects).Order("id ASC").Select(); err != nil {
		return []models.Subject{}, errors.Wrap("subject.AddSubject", "Couldn't get subjects from db", err)
	}
	return subjects, nil
}

// GetSubjects get all subjects of user
func (s Subject) GetSubjects(userID int) ([]models.Subject, error) {
	subjects := make([]models.Subject, 0)
	if err := s.Db.Model(&subjects).Order("id ASC").Where("assistant_id = ?", userID).Select(); err != nil {
		return []models.Subject{}, errors.Wrap("subject.AddSubject", "Couldn't get subjects from db", err)
	}
	return subjects, nil
}

// EditSubject edits subject based on subject.ID, all fields will be updated
func (s Subject) EditSubject(subject models.Subject, userID int) (models.Subject, error) {
	return models.Subject{}, nil
}
