package controllers

import (
	"testing"

	"gitlab.com/kevv/backpractical/db/models"
)

var pgdb = setUpTestDb()
var authCont = Auth{pgdb}
var subjectCont = Subject{pgdb}

func TestAddSubject(t *testing.T) {
	storedAssist := models.Assistant{
		Name: "One Two", Email: "one@one.com", Password: "1234567",
	}
	authCont.AddAssistant(&storedAssist)
	subject := models.Subject{
		Name: "Assembler", Sigla: "INF-154",
	}
	storedSubjects, err := subjectCont.AddSubject(&subject, storedAssist.ID)
	assertNoError(t, err)
	if len(storedSubjects) != 1 {
		t.Errorf("Expected 1 subject stored, got: %x", len(storedSubjects))
		return
	}
	if storedSubjects[0].ID != 1 {
		t.Errorf("Expected subject to be saved with id 1, but got: %x", storedSubjects[0].ID)
	}
	if storedSubjects[0].AssistantID != 1 {
		t.Errorf("Expected subject's assistant ID to be saved with id 1, but got: %x", storedSubjects[0].AssistantID)
	}
}

func TestGetSubjects(t *testing.T) {
	subjects, err := subjectCont.GetSubjects(1)
	assertNoError(t, err)
	if len(subjects) != 1 {
		t.Errorf("expected 1 subject stored, got: %x", len(subjects))
	}
	if subjects[0].AssistantID != 1 ||
		subjects[0].Name != "Assembler" ||
		subjects[0].Sigla != "INF-154" {
		t.Errorf("the expected subject didn't math with the original one, got: %v", subjects[0])
	}
}
