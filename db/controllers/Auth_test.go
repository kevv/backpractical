package controllers

import (
	"testing"

	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
)

var auth = Auth{setUpTestDb()}

func TestAddAssistant(t *testing.T) {

	t.Run("should register a new assistant", func(t *testing.T) {
		assistant := models.Assistant{
			Name: "One Two", Email: "one@one.com", Password: "1234567",
		}
		err := auth.AddAssistant(&assistant)

		assertNoError(t, err)
		// storedAssistant must have 1 as ID
		if assistant.ID != 1 {
			t.Errorf("Expected stored assistant ID should be 1, got: %x", assistant.ID)
		}
	})
	t.Run("should return error if email is already in use", func(t *testing.T) {
		assistant := models.Assistant{
			Name: "One Two", Email: "one@one.com", Password: "1234567",
		}
		err := auth.AddAssistant(&assistant)
		expectedError := errors.New(
			AuthErrs.EmailOp, AuthErrs.EmailDes,
		)
		compareExpectedErrors(t, err, expectedError)
	})
}

func TestLogin(t *testing.T) {
	t.Run("Should return assistant on success", func(t *testing.T) {
		assistant := models.Assistant{
			Name: "One Two", Email: "one@one.com", Password: "1234567",
		}
		storedAssist, err := auth.Login(assistant.Email, assistant.Password)
		assertNoError(t, err)
		if storedAssist.ID != 1 && storedAssist.Name == assistant.Name {
			t.Errorf("Expected stored assistant ID should be 1, got: %x", assistant.ID)
		}
	})
	t.Run("should return an error if email or password is wrong", func(t *testing.T) {
		// email error
		_, err1 := auth.Login("somerandom@email.com", "1234567")
		expErr := errors.New(AuthErrs.LoginOp, AuthErrs.LoginDes)
		// password error
		compareExpectedErrors(t, err1, expErr)
		_, err2 := auth.Login("one@one.com", "7654321")
		compareExpectedErrors(t, err2, expErr)
	})
}
