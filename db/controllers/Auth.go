package controllers

import (
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/kevv/backpractical/common"
	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
	"golang.org/x/crypto/bcrypt"
)

// AuthErrs stores all auth user posible errors (No server Errors)
var AuthErrs = struct {
	EmailOp      string
	EmailDes     string
	LoginOp      string
	LoginDes     string
	WrongCredOp  string
	WrongCredDes string
}{
	EmailOp:      "auth.ExistEmail",
	EmailDes:     "Email already registered",
	LoginOp:      "auth.WrongCredentials",
	LoginDes:     "Email or Password incorrect",
	WrongCredOp:  "auth.WrongCredentials",
	WrongCredDes: "Email or password is incorrect",
}

// Auth all actions to hangle authentication
type Auth struct {
	Db *pg.DB
}

// AddAssistant saves a new asssistant in DB
func (a *Auth) AddAssistant(assistant *models.Assistant) error {
	// check if user exists
	existsAssist, err := a.Db.Model(assistant).Where("email = ?email").Exists()

	if err != nil {
		return errors.Wrap("auth.CheckingEmail", "Could not check email in AddAssistant Method", err)
	}
	if existsAssist {
		return errors.New(AuthErrs.EmailOp, AuthErrs.EmailDes)
	}
	password, err := common.HashPass(assistant.Password)
	if err != nil {
		return errors.Wrap("auth.HashPass", "Error hashing password in AddAssistant", err)
	}
	assistant.CreatedAt = time.Now()
	assistant.UpdatedAt = time.Now()
	assistant.Password = password
	if _, err := a.Db.Model(assistant).Returning("*").Insert(); err != nil {
		return errors.Wrap("auth.InsertAssistant", "Could not save new assistant in AddAssistant Method", err)
	}
	return nil
}

// Login check login data and returns Assistant data and token
func (a *Auth) Login(email, password string) (models.Assistant, error) {
	assist := models.Assistant{Email: email}
	exists, err := a.Db.Model(&assist).Where("email = ?email").Exists()
	if err != nil {
		return assist, errors.Wrap("auth.CheckingEmail", "Could not check email in Login method", err)
	}
	if !exists {
		return assist, errors.New(AuthErrs.WrongCredOp, AuthErrs.WrongCredDes)
	}
	a.Db.Model(&assist).Where("email = ?email").Select()
	if err := bcrypt.CompareHashAndPassword([]byte(assist.Password), []byte(password)); err != nil {
		return assist, errors.New(AuthErrs.WrongCredOp, AuthErrs.WrongCredDes)
	}
	return assist, nil
}
