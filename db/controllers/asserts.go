package controllers

import (
	"testing"

	"github.com/go-pg/pg"
	"gitlab.com/kevv/backpractical/config"
	"gitlab.com/kevv/backpractical/db"
	"gitlab.com/kevv/backpractical/errors"
)

func setUpTestDb() *pg.DB {
	env, _ := config.New("../../.env")
	pgDB := pg.Connect(&pg.Options{
		User:     env.DB.Username,
		Database: env.DB.DBName,
		Password: env.DB.Password,
	})
	db.CreateTestTables(pgDB)
	return pgDB
}

func assertNoError(t *testing.T, e error) {
	t.Helper()
	if e != nil {
		t.Errorf("Error should be nil, got: %q", e.Error())
	}
}

func compareExpectedErrors(t *testing.T, e1, e2 error) {
	t.Helper()
	err1, okE1 := e1.(*errors.Error)
	err2, okE2 := e1.(*errors.Error)
	if !okE1 {
		t.Errorf("Error does not implement errors.Error struct, got type: %T", e1)
		return
	}
	if !okE2 {
		t.Errorf("Error does not implement errors.Error struct, got type: %T", e2)
		return
	}
	if err1.Detail != err2.Detail ||
		err1.Op != err2.Op ||
		err1.Err != nil ||
		err2.Err != nil {
		t.Errorf(
			"Should throw and expected error, want: %+v, got: %+v",
			err1, err2,
		)
	}
}
