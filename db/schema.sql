drop table if exists work;
drop table if exists subject;
drop table if exists assistant;

drop table if exists answer;
drop table if exists work_done;
drop table if exists student;

drop type if exists scores;

create type scores as enum ('right', 'good', 'wrong');

create table assistant (
  id serial primary key,
  name varchar,
  email varchar,
  password varchar,
  created_at timestamp,
  updated_at timestamp
);

create table subject (
  id serial primary key,
  assistant_id int not null,
  name varchar,
  sigla varchar,
  created_at timestamp,
  updated_at timestamp,
  foreign key (assistant_id) references assistant(id)
);

create table work (
  id serial primary key,
  subject_id int not null,
  name varchar,
  questions int,
  limit_date date,
  created_at timestamp,
  updated_at timestamp,
  foreign key (subject_id) references subject(id)
);

create table student (
  id serial primary key,
  ci int,
  name varchar,
  last_name varchar,
  created_at timestamp,
  updated_at timestamp
);

create table work_done (
  id serial primary key,
  student_id int not null,
  work_id int not null,
  created_at timestamp,
  updated_at timestamp,
  foreign key (student_id) references student(id),
  foreign key (work_id) references work(id)
);

create table answer (
  id serial primary key,
  work_done_id int not null,
  number int,
  score scores,
  created_at timestamp,
  updated_at timestamp,
  foreign key (work_done_id) references work_done(id)
);
