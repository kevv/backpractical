package db

import "gitlab.com/kevv/backpractical/db/models"

// AuthController interface representing the methods of controllers
type AuthController interface {
	AddAssistant(assistant *models.Assistant) error
	Login(email, password string) (models.Assistant, error)
}

// SubjectController interface with all actions related to Subjects
type SubjectController interface {
	AddSubject(subject *models.Subject, userID int) ([]models.Subject, error)
	GetSubjects(userID int) ([]models.Subject, error)
	EditSubject(subject models.Subject, userID int) (models.Subject, error)
}
