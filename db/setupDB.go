package db

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/kevv/backpractical/db/models"
)

var dbModels []interface{} = []interface{}{
	&models.Assistant{},
	&models.Subject{},
	&models.Work{},
	&models.Student{},
	&models.Homework{},
	&models.Answer{},
}

// CreateTables creates all tables based in models
func CreateTables(db *pg.DB) error {
	for _, model := range dbModels {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
			IfNotExists:   true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

// CreateTestTables creates all temporal tables based in models for testing purposes
func CreateTestTables(db *pg.DB) error {
	orm.SetTableNameInflector(func(s string) string {
		return "test_" + s
	})
	for _, model := range dbModels {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
			Temp:          true,
			IfNotExists:   true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
