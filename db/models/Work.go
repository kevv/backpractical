package models

import "time"

// Work struct
type Work struct {
	ID        int
	Name      string
	Questions int
	LimitDate time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
	SubjectID int
	Subject   *Subject
}
