package models

import "time"

// Subject struct
type Subject struct {
	ID          int
	Name        string
	Sigla       string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	AssistantID int
	Assistant   *Assistant
}
