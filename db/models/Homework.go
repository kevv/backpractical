package models

import "time"

// Homework stuct
type Homework struct {
	ID        int
	CreatedAt time.Time
	UpdatedAt time.Time
	StudentID int
	Student   *Student
	WorkID    int
	Work      *Work
}
