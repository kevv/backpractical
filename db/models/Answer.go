package models

import "time"

// Answer struct
type Answer struct {
	ID         int       `json:"id"`
	Number     int       `json:"number"`
	Score      string    `json:"score"`
	CreatedAt  time.Time `json:"createdAt"`
	UpdatedAt  time.Time `json:"updatedAt"`
	HomeworkID int
	Homework   *Homework `json:"homework"`
}
