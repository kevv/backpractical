package models

import "time"

// Student struct
type Student struct {
	ID        int
	Ci        int
	Name      string
	LastName  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
