package common

import (
	"regexp"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/kevv/backpractical/errors"
	"golang.org/x/crypto/bcrypt"
)

// AuthClaims the data to be encoded in jwt
type AuthClaims struct {
	Email string
	ID    int
	jwt.StandardClaims
}

// Errs stores all common user posible errors (No server Errors)
var Errs = struct {
	PassOp  string
	PassDes string
}{
	PassOp:  "auth.BadPassword",
	PassDes: "Password not accepted",
}

// CheckPass Check if password follows the Regex
func CheckPass(password string) error {
	match, err := regexp.MatchString(`[a-zA-Z0-9_.*#$]{6,25}$`, password)
	if err != nil {
		return errors.Wrap("checkpass.BadRegexString", "The regex string is invalid", err)
	}
	if !match {
		return errors.New(Errs.PassOp, Errs.PassDes)
	}
	return nil
}

// HashPass hashes the current password with bcrypt algorithm
func HashPass(password string) (string, error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", errors.Wrap("hashpass.GeneratingHash", "Can't generate hashed password", err)
	}
	return string(hashed), nil
}

// GenToken Generates a token
func GenToken(key, email string, id int) (string, error) {
	tokenClaims := jwt.MapClaims{
		"email": email,
		"id":    id,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaims)
	str, err := token.SignedString([]byte(key))
	return str, err
}
