package config

import (
	"os"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

// DBConfig represents .env database related ENV variables
type DBConfig struct {
	Username string
	DBName   string
	Password string
}

// GinConfig env variables to config gin.
type GinConfig struct {
	Mode   string
	JwtKey string
}

// Config represents all config variables
type Config struct {
	DB  DBConfig
	Gin GinConfig
}

// New returns a Config struct with all ENV variables
func New(path string) (Config, error) {
	err := godotenv.Load(path)
	if err != nil {
		return Config{}, err
	}
	return Config{
		DB: DBConfig{
			Username: getEnv("DB_USER", ""),
			DBName:   getEnv("DB_DATABASE", ""),
			Password: getEnv("DB_PASSWORD", ""),
		},
		Gin: GinConfig{
			Mode:   getEnv("GIN_MODE", "release"),
			JwtKey: getEnv("JWT_KEY", ""),
		},
	}, nil
}

func getEnv(key, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

// Helper to read an environment variable into a bool or return default value
func getEnvAsBool(name string, defaultVal bool) bool {
	valStr := getEnv(name, "")
	if val, err := strconv.ParseBool(valStr); err == nil {
		return val
	}

	return defaultVal
}

// Helper to read an environment variable into a string slice or return default value
func getEnvAsSlice(name string, defaultVal []string, sep string) []string {
	valStr := getEnv(name, "")

	if valStr == "" {
		return defaultVal
	}

	val := strings.Split(valStr, sep)

	return val
}
