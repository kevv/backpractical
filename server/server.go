package server

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"gitlab.com/kevv/backpractical/config"
	"gitlab.com/kevv/backpractical/db"
	"gitlab.com/kevv/backpractical/errors"
)

// Controllers struct to store all controllers
type Controllers struct {
	Auth    db.AuthController
	Subject db.SubjectController
}

// Server structs representing the whole server
type Server struct {
	Db   *pg.DB
	Ctx  context.Context
	Gin  *gin.Engine
	Cont Controllers
	Env  config.Config
}

// ErrorStruct represents an http response error struct
type ErrorStruct struct {
	Action     string      `json:"action"`
	Message    string      `json:"message"`
	StackTrace []errors.Op `json:"stackTrace"`
}

// ResponseStruct struct to be used in all reponses
type ResponseStruct struct {
	Data  interface{}  `json:"data"`
	Error *ErrorStruct `json:"error"`
}

// Resp  function to return a response acording to ReponseStruct
func Resp(data interface{}) ResponseStruct {
	resp := ResponseStruct{Data: data, Error: nil}
	return resp
}

// ErrResp handles an error response.
func ErrResp(c *gin.Context, err error, code int) {
	resp := ResponseStruct{Data: nil}
	errResponse := &ErrorStruct{}
	errCasted, ok := err.(*errors.Error)
	// just in case we check if err type is different of error.Error
	if !ok {
		errResponse.Action = "generatingResponse.checkingError"
		errResponse.Message = "The error can't be converted to Error type"
		errResponse.StackTrace = nil
		resp.Error = errResponse
		c.JSON(http.StatusInternalServerError, resp)
		return
	}
	// if error is not expected we get 500 error
	errResponse.Action = string(errCasted.Op)
	errResponse.Message = string(errCasted.Detail)
	if !errCasted.IsExpected() {
		errResponse.Message += "Error: " + errCasted.Err.Error()
		errResponse.StackTrace = errCasted.Ops()
		resp.Error = errResponse
		c.JSON(http.StatusInternalServerError, resp)
		return
	}
	// expected error, we set response code
	resp.Error = errResponse
	c.JSON(code, resp)
}

// NewServer creates a new server with all routes loaded
func NewServer(engine *gin.Engine, env config.Config) *Server {
	s := &Server{Gin: engine, Env: env}
	s.routes()
	return s
}

// Run Makes the server
func (s *Server) Run() {
	s.Gin.Run()
}

// COMMON RESPONSES

// ErrNoUserID returns User ID not found in context (header)
func ErrNoUserID(c *gin.Context, action string) {
	ErrResp(
		c,
		errors.New("getSubjects.getID", "Can't get ID from context"),
		http.StatusInternalServerError,
	)
}
