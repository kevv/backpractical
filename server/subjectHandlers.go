package server

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
)

func (s *Server) handleNewSubject() gin.HandlerFunc {
	type reqStruct struct {
		Name  string `json:"name"`
		Sigla string `json:"sigla"`
	}
	type respStruct struct {
		Subjects []models.Subject `json:"subjects"`
	}
	return func(c *gin.Context) {
		uID, exist := c.Get("id")
		if !exist {
			ErrNoUserID(c, "newSubject.getID")
		}
		subject := models.Subject{}
		c.ShouldBindJSON(&subject)
		subjects, err := s.Cont.Subject.AddSubject(&subject, uID.(int))
		if err != nil {
			ErrResp(c, err, http.StatusInternalServerError)
		}
		resp := respStruct{Subjects: subjects}
		c.JSON(http.StatusCreated, Resp(resp))
	}
}

func (s *Server) handleGetSubjects() gin.HandlerFunc {
	type respStruct struct {
		Subjects []models.Subject `json:"subjects"`
	}
	return func(c *gin.Context) {
		uID, exist := c.Get("id")
		if !exist {
			ErrNoUserID(c, "getSubjects.GetID")
		}
		subjects, err := s.Cont.Subject.GetSubjects(uID.(int))
		if err != nil {
			ErrResp(c, err, http.StatusInternalServerError)
		}
		resp := respStruct{Subjects: subjects}
		c.JSON(http.StatusCreated, Resp(resp))
	}
}

func (s *Server) handleEditSubject() gin.HandlerFunc {
	type requestData struct {
		ID    string `uri:"id" binding:"required"`
		Name  string `json:"name"`
		Sigla string `json:"sigla"`
	}
	type respStruct struct {
		Subject models.Subject `json:"subject"`
	}
	return func(c *gin.Context) {
		userID, exist := c.Get("id")
		if !exist {
			ErrNoUserID(c, "editSubject.GetID")
		}
		reqData := requestData{}
		if err := c.ShouldBindUri(&reqData); err != nil {
			ErrResp(c, err, http.StatusInternalServerError)
		}
		if err := c.ShouldBindJSON(&reqData); err != nil {
			ErrResp(c, err, http.StatusInternalServerError)
		}
		subjectID, err := strconv.Atoi(reqData.ID)
		if err != nil {
			ErrResp(c, errors.New("editSubject.ParseID", "Can not parse requested id to int"), http.StatusBadRequest)
		}
		subject := models.Subject{
			ID:    subjectID,
			Name:  reqData.Name,
			Sigla: reqData.Sigla,
		}
		editedSubject, err := s.Cont.Subject.EditSubject(subject, userID.(int))
		if err != nil {
			ErrResp(c, err, http.StatusBadRequest)
		}
		c.JSON(http.StatusOK, Resp(respStruct{editedSubject}))
	}
}
