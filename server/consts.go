package server

// Consts common contants used in project
var consts = struct {
	ContentType   string
	AppJSON       string
	Authorization string
}{
	ContentType:   "Content-Type",
	AppJSON:       "application/json; charset=utf-8",
	Authorization: "Authorization",
}
