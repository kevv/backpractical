package server

var routes = struct {
	login       string
	register    string
	newSubject  string
	getSubjects string
	editSubject func(id string) string
}{
	"/login",
	"/register",
	"/subject",
	"/subjects",
	func(id string) string {
		if id == "" {
			return "/subject/:id"
		}
		return "/subject/" + id
	},
}

func (s *Server) routes() {
	token := s.Env.Gin.JwtKey
	s.Gin.POST(routes.register, s.handleRegister())
	s.Gin.POST(routes.login, s.handleLogin())
	s.Gin.POST(routes.newSubject, Auth(token), s.handleNewSubject())
	s.Gin.GET(routes.getSubjects, Auth(token), s.handleGetSubjects())
	s.Gin.PATCH(routes.editSubject(""), Auth(token), s.handleEditSubject())
}
