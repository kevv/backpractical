package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/common"
	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
)

func (s *Server) handleLogin() gin.HandlerFunc {
	type reqStruct struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	type respStruct struct {
		Token string           `json:"token"`
		User  models.Assistant `json:"user"`
	}
	return func(c *gin.Context) {
		var cred reqStruct
		err := c.ShouldBindJSON(&cred)
		if err != nil {
			ErrResp(
				c,
				errors.Wrap("handleLogin.BindData", "Can't bind json to req struct", err),
				http.StatusInternalServerError,
			)
			return
		}
		assistant, err := s.Cont.Auth.Login(cred.Email, cred.Password)
		if err != nil {
			ErrResp(c, err, http.StatusBadRequest)
			return
		}
		token, err := common.GenToken(s.Env.Gin.JwtKey, assistant.Email, assistant.ID)

		if err != nil {
			ErrResp(c, errors.Wrap("handleLogin.Token", "Error generating token", err), http.StatusInternalServerError)
		}
		resp := respStruct{
			User:  assistant,
			Token: token,
		}
		c.JSON(http.StatusOK, Resp(resp))
	}
}

func (s *Server) handleRegister() gin.HandlerFunc {
	type respStruct struct {
		Token string           `json:"token"`
		User  models.Assistant `json:"user"`
	}
	return func(c *gin.Context) {
		var assistant models.Assistant
		err := c.ShouldBindJSON(&assistant)
		if err != nil {
			ErrResp(c, errors.Wrap("handleRegister.BindAssistantJson", "Can't parse request JSON", err), http.StatusInternalServerError)
			return
		}
		err = common.CheckPass(assistant.Password)
		if err != nil {
			ErrResp(c, err, http.StatusBadRequest)
			return
		}
		if err := s.Cont.Auth.AddAssistant(&assistant); err != nil {
			ErrResp(c, err, http.StatusConflict)
			return
		}
		token, err := common.GenToken(s.Env.Gin.JwtKey, assistant.Email, assistant.ID)

		if err != nil {
			ErrResp(c, errors.Wrap("handleRegister.Token", "Error generating token", err), http.StatusInternalServerError)
		}

		resp := respStruct{
			User:  assistant,
			Token: token,
		}

		c.JSON(http.StatusCreated, Resp(resp))
	}
}
