package server

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/common"
)

func TestMiddleware(t *testing.T) {
	t.Run("Auth() Should run handler with context values", func(t *testing.T) {
		email := "one@one.com"
		id := 1
		gin.SetMode(gin.TestMode)
		server := NewServer(gin.New(), mockEnv)
		next := func(c *gin.Context) {
			contextEmail, existsEmail := c.Get("email")
			contextID, existsID := c.Get("id")
			if !existsEmail {
				t.Error("Expected exist email value in context")
			}
			if !existsID {
				t.Error("Expected exist email value in context")
			}
			if contextEmail != email {
				t.Errorf("context email value doesn't match, got: %q, want %q", contextEmail, email)
			}
			if contextID != id {
				t.Errorf("context id doesn't match, got: %f, want %x", contextID, id)
			}
		}
		token, _ := common.GenToken("Secret", email, id)

		server.Gin.GET("/", Auth("Secret"), next)

		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, "/", nil)
		req.Header.Set(consts.ContentType, consts.AppJSON)
		req.Header.Set(consts.Authorization, token)

		server.Gin.ServeHTTP(w, req)
		fmt.Println(w.Body.String())
	})
}
