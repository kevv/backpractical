package server

import (
	"fmt"
	"net/http/httptest"
	"testing"
)

// assertStatusCode check if the status code is right
func assertStatusCode(t *testing.T, w *httptest.ResponseRecorder, want int) {
	t.Helper()
	if w.Code != want {
		t.Errorf("Did not get correct status, got %d, want %d", w.Code, want)
	}
}

// assertResponseBody check for reponse body
func assertResponseBody(t *testing.T, w *httptest.ResponseRecorder, want string) {
	t.Helper()
	bodyString := w.Body.String()
	if bodyString != want {
		t.Errorf("Response bodies doesn't match, got: \n%q\nwant: \n%q", bodyString, want)
	}
}

// assertContentType checks the Content-Type
func assertContentType(t *testing.T, w *httptest.ResponseRecorder, contentType string) {
	t.Helper()
	respContentType := w.Header().Get(consts.ContentType)
	if respContentType != contentType {
		t.Errorf(fmt.Sprintf("Response %s must be: %s, instead got: %s", consts.ContentType, contentType, respContentType))
	}
}
