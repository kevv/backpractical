package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/common"
	"gitlab.com/kevv/backpractical/db/models"
)

type subjectTestCont struct {
	subjects []models.Subject
}

func (s *subjectTestCont) AddSubject(subject *models.Subject, id int) ([]models.Subject, error) {
	subject.AssistantID = id
	subject.ID = len(s.subjects) + 1
	s.subjects = append(s.subjects, *subject)
	return s.subjects, nil
}

func (s *subjectTestCont) GetSubjects(id int) ([]models.Subject, error) {
	subjects := []models.Subject{}
	for _, subject := range s.subjects {
		if subject.AssistantID == id {
			subjects = append(subjects, subject)
		}
	}
	return subjects, nil
}

func (s *subjectTestCont) EditSubject(subject models.Subject, userID int) (models.Subject, error) {
	newSubject := models.Subject{
		ID:          subject.ID,
		Name:        subject.Name,
		Sigla:       subject.Sigla,
		AssistantID: userID,
	}
	return newSubject, nil
}

func TestRegisterSubject(t *testing.T) {
	dummyAssist := models.Assistant{
		ID: 1, Name: "Kevv", Email: "kevv@gmail.com", Password: "1234567",
	}
	token, _ := common.GenToken(mockEnv.Gin.JwtKey, dummyAssist.Email, dummyAssist.ID)
	gin.SetMode(gin.TestMode)
	server := NewServer(gin.New(), mockEnv)
	server.Cont.Subject = &subjectTestCont{}

	type requestVars struct {
		Name  string `json:"name"`
		Sigla string `json:"sigla"`
	}
	t.Run("It should register a new subject", func(t *testing.T) {
		reqVars := requestVars{"Assembler", "INF-000"}
		reqJSON, _ := json.Marshal(reqVars)

		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPost, routes.newSubject, bytes.NewBuffer(reqJSON))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		req.Header.Set(consts.Authorization, token)

		server.Gin.ServeHTTP(w, req)

		expectedResp, _ := json.Marshal(Resp(struct {
			Subjects []models.Subject `json:"subjects"`
		}{
			Subjects: []models.Subject{
				{
					ID:          1,
					Name:        reqVars.Name,
					Sigla:       reqVars.Sigla,
					AssistantID: dummyAssist.ID,
				},
			},
		}))

		assertStatusCode(t, w, http.StatusCreated)
		assertContentType(t, w, consts.AppJSON)
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(expectedResp)))
	})
}
func TestListSubject(t *testing.T) {
	dummyAssist := models.Assistant{
		ID: 1, Name: "Kevv", Email: "kevv@gmail.com", Password: "1234567",
	}
	token, _ := common.GenToken(mockEnv.Gin.JwtKey, dummyAssist.Email, dummyAssist.ID)
	gin.SetMode(gin.TestMode)
	server := NewServer(gin.New(), mockEnv)
	storedSubjects := []models.Subject{
		{ID: 1, Name: "Assembler", Sigla: "INF-154", AssistantID: 1},
		{ID: 2, Name: "Calculo", Sigla: "MAT-115", AssistantID: 2},
		{ID: 3, Name: "Algebra", Sigla: "MAT-114", AssistantID: 1},
	}
	server.Cont.Subject = &subjectTestCont{
		subjects: storedSubjects,
	}

	t.Run("Should list user subjects", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, routes.getSubjects, nil)

		req.Header.Set(consts.ContentType, consts.AppJSON)
		req.Header.Set(consts.Authorization, token)

		server.Gin.ServeHTTP(w, req)

		expectedResp, _ := json.Marshal(Resp(struct {
			Subjects []models.Subject `json:"subjects"`
		}{
			Subjects: []models.Subject{
				{ID: 1, Name: "Assembler", Sigla: "INF-154", AssistantID: 1},
				{ID: 3, Name: "Algebra", Sigla: "MAT-114", AssistantID: 1},
			},
		}))

		assertStatusCode(t, w, http.StatusCreated)
		assertContentType(t, w, consts.AppJSON)
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(expectedResp)))
	})
}

func TestEditSubject(t *testing.T) {
	dummyAssist := models.Assistant{
		ID: 1, Name: "Kevv", Email: "kevv@gmail.com", Password: "1234567",
	}
	token, _ := common.GenToken(mockEnv.Gin.JwtKey, dummyAssist.Email, dummyAssist.ID)
	gin.SetMode(gin.TestMode)
	server := NewServer(gin.New(), mockEnv)
	storedSubjects := []models.Subject{
		{ID: 1, Name: "Assembler", Sigla: "INF-154", AssistantID: 1},
		{ID: 3, Name: "Algebra", Sigla: "MAT-114", AssistantID: 1},
	}
	server.Cont.Subject = &subjectTestCont{
		subjects: storedSubjects,
	}
	type requestVars struct {
		Name  string `json:"name"`
		Sigla string `json:"sigla"`
	}
	t.Run("It should update subject (1 field)", func(t *testing.T) {
		reqJSON, _ := json.Marshal(requestVars{"Assembler Updated", "INF-154"})

		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPatch, routes.editSubject("1"), bytes.NewBuffer(reqJSON))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		req.Header.Set(consts.Authorization, token)

		server.Gin.ServeHTTP(w, req)
		expectedResp, _ := json.Marshal(Resp(struct {
			Subject models.Subject `json:"subject"`
		}{
			models.Subject{ID: 1, Name: "Assembler Updated", Sigla: "INF-154", AssistantID: 1},
		}))

		assertStatusCode(t, w, http.StatusOK)
		assertContentType(t, w, consts.AppJSON)
		assertResponseBody(t, w, string(expectedResp)+"\n")
	})
	t.Run("It should update subject (all fields)", func(t *testing.T) {
		reqJSON, _ := json.Marshal(requestVars{"Assembler Updated", "NEW-000"})

		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPatch, routes.editSubject("1"), bytes.NewBuffer(reqJSON))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		req.Header.Set(consts.Authorization, token)

		server.Gin.ServeHTTP(w, req)
		expectedResp, _ := json.Marshal(Resp(struct {
			Subject models.Subject `json:"subject"`
		}{
			models.Subject{ID: 1, Name: "Assembler Updated", Sigla: "NEW-000", AssistantID: 1},
		}))

		assertStatusCode(t, w, http.StatusOK)
		assertContentType(t, w, consts.AppJSON)
		assertResponseBody(t, w, string(expectedResp)+"\n")
	})
}
