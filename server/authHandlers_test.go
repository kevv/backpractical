package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/common"
	"gitlab.com/kevv/backpractical/config"
	"gitlab.com/kevv/backpractical/db/controllers"
	"gitlab.com/kevv/backpractical/db/models"
	"gitlab.com/kevv/backpractical/errors"
)

type authTestController struct {
	assistants []*models.Assistant
}

func (a *authTestController) AddAssistant(assistant *models.Assistant) error {

	for _, assist := range a.assistants {
		if assist.Email == assistant.Email {
			return errors.New(
				controllers.AuthErrs.EmailOp,
				controllers.AuthErrs.EmailDes,
			)
		}
	}
	err := common.CheckPass(assistant.Password)
	if err != nil {
		return errors.New(common.Errs.PassOp, common.Errs.PassDes)
	}
	assistant.ID = len(a.assistants) + 1
	a.assistants = append(a.assistants, assistant)
	return nil
}

func (a *authTestController) Login(email, password string) (models.Assistant, error) {
	for _, assist := range a.assistants {
		if assist.Email == email && assist.Password == password {
			return *assist, nil
		}
	}
	da := models.Assistant{}
	return da, errors.New(
		controllers.AuthErrs.LoginOp,
		controllers.AuthErrs.LoginDes,
	)
}

/* For every request, we need to check
response code (http status)
request and response Content-Type, or some header
expected response body
context variables if is needed
*/

var mockEnv config.Config = config.Config{
	Gin: config.GinConfig{
		JwtKey: "Secret",
	},
}

func TestRegisterAssistant(t *testing.T) {
	dummyAssistant := models.Assistant{
		Name:     "Jhon",
		Email:    "jhon@gmail.com",
		Password: "1234567",
	}
	requestBody, _ := json.Marshal(dummyAssistant)
	gin.SetMode(gin.TestMode)
	server := NewServer(gin.Default(), mockEnv)
	authCtrl := authTestController{}
	server.Cont.Auth = &authCtrl

	t.Run("It should register a new assistant", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPost, routes.register, bytes.NewBuffer(requestBody))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		server.Gin.ServeHTTP(w, req)

		assertStatusCode(t, w, http.StatusCreated)
		assertContentType(t, w, consts.AppJSON)
		dummyAssistant.ID = 1
		token, _ := common.GenToken(server.Env.Gin.JwtKey, dummyAssistant.Email, 1)
		expectedResponse, _ := json.Marshal(Resp(struct {
			Token string           `json:"token"`
			User  models.Assistant `json:"user"`
		}{
			Token: token,
			User:  dummyAssistant,
		}))
		// We need to add and \n because gin c.JSON function does.
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(expectedResponse)))
		if len(authCtrl.assistants) != 1 {
			t.Errorf("Expected to have 1 stored assistant, got: %d", len(authCtrl.assistants))
		}
	})
	t.Run("It must show error if email already registered", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPost, routes.register, bytes.NewBuffer(requestBody))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		server.Gin.ServeHTTP(w, req)

		assertStatusCode(t, w, http.StatusConflict)
		assertContentType(t, w, consts.AppJSON)
		responseStringExpected, _ := json.Marshal(ResponseStruct{
			Data: nil,
			Error: &ErrorStruct{
				Action:     controllers.AuthErrs.EmailOp,
				Message:    controllers.AuthErrs.EmailDes,
				StackTrace: nil,
			},
		})
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(responseStringExpected)))
	})
	t.Run("Returns error when password doesn't follow regex [a-zA-Z0-9_.*#$]{6,25}$", func(t *testing.T) {
		w := httptest.NewRecorder()
		reqString, _ := json.Marshal(models.Assistant{Name: "Jhon", Email: "jhon2@gmail.com", Password: "12345"})
		req, _ := http.NewRequest(http.MethodPost, routes.register, bytes.NewBuffer(reqString))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		server.Gin.ServeHTTP(w, req)

		assertStatusCode(t, w, http.StatusBadRequest)
		assertContentType(t, w, consts.AppJSON)
		responseStringExpected, _ := json.Marshal(ResponseStruct{
			Data: nil,
			Error: &ErrorStruct{
				Action:     common.Errs.PassOp,
				Message:    common.Errs.PassDes,
				StackTrace: nil,
			},
		})
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(responseStringExpected)))
	})
}

func TestLoginAssistant(t *testing.T) {
	dummyAssistant := models.Assistant{
		Name:     "Jhon",
		Email:    "jhon@gmail.com",
		Password: "1234567",
	}
	gin.SetMode(gin.ReleaseMode)
	server := NewServer(gin.Default(), mockEnv)
	authCtrl := authTestController{
		[]*models.Assistant{&dummyAssistant},
	}
	type loginCred struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	server.Cont.Auth = &authCtrl
	t.Run("Works with correct credentials", func(t *testing.T) {
		reqCred := loginCred{
			Email:    "jhon@gmail.com",
			Password: "1234567",
		}
		reqCredJSON, _ := json.Marshal(reqCred)
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPost, routes.login, bytes.NewBuffer(reqCredJSON))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		server.Gin.ServeHTTP(w, req)

		assertStatusCode(t, w, http.StatusOK)
		assertContentType(t, w, consts.AppJSON)
		token, _ := common.GenToken(server.Env.Gin.JwtKey, dummyAssistant.Email, 0)
		resStringExpected, _ := json.Marshal(Resp(struct {
			Token string           `json:"token"`
			User  models.Assistant `json:"user"`
		}{
			Token: token,
			User:  dummyAssistant,
		}))
		assertResponseBody(t, w, fmt.Sprintf("%s\n", resStringExpected))
	})
	t.Run("Throws an user error with wrong credentials", func(t *testing.T) {
		reqCred := loginCred{
			Email:    "random@gmail.com",
			Password: "randompass",
		}
		reqCredJSON, _ := json.Marshal(reqCred)
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodPost, routes.login, bytes.NewBuffer(reqCredJSON))

		req.Header.Set(consts.ContentType, consts.AppJSON)
		server.Gin.ServeHTTP(w, req)

		assertStatusCode(t, w, http.StatusBadRequest)
		assertContentType(t, w, consts.AppJSON)
		responseStringExpected, _ := json.Marshal(ResponseStruct{
			Data: nil,
			Error: &ErrorStruct{
				Action:     controllers.AuthErrs.LoginOp,
				Message:    controllers.AuthErrs.LoginDes,
				StackTrace: nil,
			},
		})
		assertResponseBody(t, w, fmt.Sprintf("%s\n", string(responseStringExpected)))
	})
}
