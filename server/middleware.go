package server

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/kevv/backpractical/errors"
)

// Auth handles token validation, it need the key to validate token
func Auth(key string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// check if Authorization header exists, returns empty string if not
		tokenStr := c.GetHeader("Authorization")
		if tokenStr == "" {
			c.Abort()
			err := errors.New("authMidd.NoAuthorization", "Authorization header is missing")
			ErrResp(c, err, http.StatusUnauthorized)
			return
		}
		// parsing and verifying token.
		token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
			return []byte(key), nil
		})
		// check if there is an error or token is invalid.
		if err != nil || !token.Valid {
			ErrResp(
				c,
				errors.Wrap("authMidd.TokenInvalid", "Can't verify token correctly", err),
				http.StatusInternalServerError,
			)
			return
		}
		// cast token.Claims to jwt.MapClaims since token.Claims stores a map[string]interface{}
		data := token.Claims.(jwt.MapClaims)
		email, emailOk := data["email"]
		id, idOk := data["id"]
		// check if we can get values from map
		if !emailOk || !idOk {
			ErrResp(
				c,
				errors.Wrap("authMidd.TokenGetValues", "Can't get token values", err),
				http.StatusInternalServerError,
			)
			return
		}
		// Set context variables to use in handlers
		c.Set("email", email)
		c.Set("id", int(id.(float64)))
		c.Next()
	}
}
