package main

import (
	"context"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"

	// For postgres connection
	_ "github.com/lib/pq"

	"gitlab.com/kevv/backpractical/config"
	"gitlab.com/kevv/backpractical/db"
	"gitlab.com/kevv/backpractical/db/controllers"
	"gitlab.com/kevv/backpractical/server"
)

func main() {
	if err := start(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func start() error {
	env, err := config.New("./.env")
	if err != nil {
		return err
	}
	gin.SetMode(env.Gin.Mode)
	pgDb := pg.Connect(&pg.Options{
		User:     env.DB.Username,
		Password: env.DB.Password,
		Database: env.DB.DBName,
	})
	if err := db.CreateTables(pgDb); err != nil {
		return err
	}

	ctx := context.Background()
	r := gin.Default()
	// TODO: check if server.Db is userful, delete if is not needed
	appServer := server.NewServer(r, env)
	appServer.Db = pgDb
	appServer.Ctx = ctx
	appServer.Cont = server.Controllers{
		Auth:    &controllers.Auth{Db: pgDb},
		Subject: &controllers.Subject{Db: pgDb},
	}

	appServer.Run()
	return nil
}
