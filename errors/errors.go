package errors

// Op Describes an operation
type Op string

// Detail Describes an error detail
type Detail string

// Error represents an error
type Error struct {
	Op     Op     // unique name operation ej. auth.Login
	Detail Detail // category of error
	Err    error  //wrapped error
}

func (e Error) Error() string {
	if e.Err != nil {
		return e.Err.Error()
	}
	return string(e.Op) + string(e.Detail)
}

// Wrap creates an error and wraps another one
func Wrap(ope, detail string, err error) error {
	e := &Error{}
	e.Op = Op(ope)
	e.Detail = Detail(detail)
	e.Err = err
	return e
}

// New creates a new error with nil Err
func New(ope, detail string) error {
	e := &Error{}
	e.Op = Op(ope)
	e.Detail = Detail(detail)
	e.Err = nil
	return e
}

// Ops returns the stack of operations for each generated error
func (e *Error) Ops() []Op {
	res := []Op{e.Op}

	subErr, ok := e.Err.(*Error)
	if !ok {
		return res
	}
	res = append(res, subErr.Ops()...)
	return res
}

// IsExpected checks if all error is expected
func (e *Error) IsExpected() bool {
	// if e.Err is nil means that the error in known
	if e.Err == nil {
		return true
	}
	// we try to cast to Error, if not, its an unknow error
	subErr, ok := e.Err.(*Error)
	if !ok {
		return false
	}
	return subErr.IsExpected()
}
